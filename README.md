<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev_tpl_namespace_root V0.3.5 -->
# __aetst__ namespace-root project

aetst namespace-root: aetst namespace-root grouping testing helper and template portions.

## aetst namespace root package use-cases

this project is maintaining all the portions (modules and sub-packages) of the aetst namespace to:

* update and deploy common outsourced files, optionally generated from templates
* merge docstrings of all portions into a single combined and cross-linked documentation
* publish documentation via Sphinx onto [ReadTheDocs](https://aetst.readthedocs.io "aetst on RTD")
* refactor multiple or all portions of this namespace simultaneously using the grm portions actions

this namespace-root package is only needed for development tasks, so never add it to the installation requirements
file (requirements.txt) of a project.

to ensure the update and deployment of outsourced files generated from the templates provided by this root package via
the [git repository manager tool](https://github.com/aedev-group/aedev_git_repo_manager), add this root package to the
development requirements file (dev_requirements.txt) of a portion project of this namespace.

the following portions are currently included in this namespace:



